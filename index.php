<?php
require './vendor/slim/slim/Slim/Slim.php';
\Slim\Slim::registerAutoloader();
require ('./sys/logar.php');
require ('./sys/contasMes.php');
require ('./sys/notificacao.php');
require ('./sys/avulsos.php');
require ('./sys/transferirReplicar.php');
require ('./sys/SalvarItem.php');
require ('./sys/salvarContas.php');

$corsOptions = array(
    "origin" => "*",
    "exposeHeaders" => array("Content-Type", "X-Requested-With", "X-authentication", "X-client"),
    "allowMethods" => array('GET', 'POST', 'PUT', 'DELETE', 'OPTIONS')
);
$cors = new \CorsSlim\CorsSlim($corsOptions);


$app = new \Slim\Slim();
$app->add($cors);
$app->response()->header('Content-Type', 'application/json;charset=utf-8');
$app->get('/', function () {
echo "SlimProdutos";
});
$app->post('/login',function() use ($app){
	$req = $app->request;
	
	$log = new login();
	$log->logarSe($req->params('email'),$req->params('senha'));
});

$app->post('/contas',function() use($app){
	$req = $app->request;
	$ano = (int) $req->params('ano'); 
	$mes = (int) $req->params('mes'); 
	$contas = new Contas();
	$contas->getContasMes($mes,$ano,1);
});

$app->post('/avulsos',function() use($app){
	$req = $app->request;
	$desc =  $req->params('desc'); 
	$valor = $req->params('valor'); 
	$dt_venc = $req->params('dt_compra'); 
	$avulsos = new Avulsos();
	$avulsos->salvarAvulsos($desc,$valor,$dt_venc,1);
});

$app->post('/controle',function() use($app){
	$req = $app->request;
	$ano =  $req->params('ano'); 
	$mes = $req->params('mes'); 
	$contas = $req->params('contas'); 
	$tipo = $req->params('tipo');
	$controle = new ControleContas();
	$controle->modificar($ano,$mes,$contas,$tipo);
});

$app->post('/salvarItem',function() use($app){
	$req = $app->request;
	$coluna =  $req->params('coluna'); 
	$item = $req->params('item'); 
	$id = $req->params('id'); 

	$itemC = new Item();
	$itemC->salvarItem($coluna,$item,$id);
});

$app->post('/salvarConta',function() use($app){
	$req = $app->request;
	$id =  $req->params('coluna'); 
	$desc = $req->params('descSaldo'); 
	$vl = $req->params('valorSaldo'); 
	$emp = $req->params('empSaldo'); 
	$venc = $req->params('data_vencSaldo'); 
	$tipoConta = $req->params('tipoSaldo'); 

	$id = $req->params('id'); 

	$conta = new Conta();
	$conta->salvar($id,$desc,$vl,$emp,$venc,$tipoConta);
});

$app->get('/notificacao',function() use($app){
	$note = new Notificacao();
	$note->getNotificacao(1);
});
$app->run();